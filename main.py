import graphscope
from graphscope.framework.loader import Loader


if __name__ == '__main__':
    graphscope.set_option(show_log=True)
    s1 = graphscope.session()
    graph = s1.g()
    graph = (
        graph.add_vertices(
            vertices="./data/movies.csv",
            label="movie",
            properties=["title", "genres"],
            vid_field=0
        )
        .add_vertices(
            vertices=("./data/ratings.csv"),
            label="user",
            vid_field=0
        )
        .add_edges(
            edges="./data/ratings.csv",
            label="rates",
            properties=["rating"],
            src_label="user",
            dst_label="movie",
            src_field=0,
            dst_field=1
        )
    )
    interactive =s1.gremlin(graph)
    q1 = interactive.execute("g.V()"
                             ".hasLabel('user')"
                             ".has('id', '7')"
                             ".outE('rates').has('rating', gte(4.5))"
                             ".inV().as('exclude')"
                             ".inE('rates').has('rating', gte(4.5))"
                             ".outV().outE('rates').has('rating', gte(4))"
                             ".inV().where(neq('exclude'))"
                             ".dedup().order().by(inE('rates').count(), decr)"
                             ".limit(10).values('title')"
                             )
    print(q1)
    s1.close()
